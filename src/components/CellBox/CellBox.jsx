import React, { useState } from 'react'
import { colors } from '../../util/colors'
import { BLUE, GREEN, RED, YELLOW } from '../../util/constants'
import './CellBox.css'

const CellBox = ({ backgroundColor, onPieceSelection, position, red, yellow, green, blue, turn, moves, isWaitingForDiceRoll }) => {

    const [highlightColor, setHighlightColor] = useState(backgroundColor);
    const [isAnimating, setIsAnimating] = useState(false);
    const [intervalId, setIntervalId] = useState();
    let shouldRenderBackgroundColor = 1;
    const applyAnimationIfNeeded = () => {
        if (shouldAnimateForSelection()) {
            if (!isAnimating) {
                setIsAnimating(true);
                setIntervalId(setInterval(() => {
                    let color = backgroundColor === colors.white ? '#999' : colors.white;
                    shouldRenderBackgroundColor++;
                    shouldRenderBackgroundColor % 2 === 0 ? setHighlightColor(backgroundColor) : setHighlightColor(color);
                }, 400))
            }
        } else {
            clearInterval(intervalId);
            if (isAnimating) {
                setIsAnimating(false);
                setHighlightColor(color);
            }
        }
    }

    const shouldAnimateForSelection = () => {
        let playerToCheckFor = turn === RED ? red : turn === YELLOW ? yellow : turn === GREEN ? green : turn === BLUE ? blue : undefined;
        return positionMatchesPlayerPosition(playerToCheckFor) && isMovePossibleFromCurrentPosition()
    }

    const positionMatchesPlayerPosition = (playerToCheckFor) => {
        const { one, two, three, four } = playerToCheckFor.pieces;

        return one.position === position ||
            two.position === position ||
            three.position === position ||
            four.position === position
    }
    const shouldRenderPiece = () => {
        return red.pieces.one.position === position ||
            red.pieces.two.position === position ||
            red.pieces.three.position === position ||
            red.pieces.four.position === position ||

            yellow.pieces.one.position === position ||
            yellow.pieces.two.position === position ||
            yellow.pieces.three.position === position ||
            yellow.pieces.four.position === position ||

            green.pieces.one.position === position ||
            green.pieces.two.position === position ||
            green.pieces.three.position === position ||
            green.pieces.four.position === position ||

            blue.pieces.one.position === position ||
            blue.pieces.two.position === position ||
            blue.pieces.three.position === position ||
            blue.pieces.four.position === position
    }

    const getPieceColor = () => {
        let matched = [];
        red.pieces.one.position === position && matched.push(red.pices.one)
        red.pieces.two.position === position && matched.push(red.pices.two)
        red.pieces.three.position === position && matched.push(red.pices.three)
        red.pieces.four.position === position && matched.push(red.pices.four)

        yellow.pieces.one.position === position && matched.push(yellow.pices.one)
        yellow.pieces.two.position === position && matched.push(yellow.pices.two)
        yellow.pieces.three.position === position && matched.push(yellow.pices.three)
        yellow.pieces.four.position === position && matched.push(yellow.pices.four)

        green.pieces.one.position === position && matched.push(green.pices.one)
        green.pieces.two.position === position && matched.push(green.pices.two)
        green.pieces.three.position === position && matched.push(green.pices.three)
        green.pieces.four.position === position && matched.push(green.pices.four)

        blue.pieces.one.position === position && matched.push(blue.pices.one)
        blue.pieces.two.position === position && matched.push(blue.pices.two)
        blue.pieces.three.position === position && matched.push(blue.pices.three)
        blue.pieces.four.position === position && matched.push(blue.pices.four)

        let colorToReturn = colors.white;
        let updateTime = 0;

        matched.forEach((matchPiece) => {
            if (matchPiece.updateTime > updateTime) {
                updateTime = matchPiece.updateTime;
                colorToReturn = matchPiece.color === RED ? red.color : matchPiece.color === YELLOW ? yellow.color : matchPiece.color === GREEN ? green.color : matchPiece.color === BLUE ? blue.color : undefined;
            }
        })

        return colorToReturn

    }

    const getPiece = () => {
        let playerToCheckFor = turn === RED ? red : turn === YELLOW ? yellow : turn === GREEN ? green : turn === BLUE ? blue : undefined;
        const { one, two, three, four } = playerToCheckFor.pieces;

        switch (position) {
            case one.position:
                return one
            case two.position:
                return two
            case three.position:
                return three
            case four.position:
                return four
            default:
                return
        }

    }

    const isMovePossibleFromCurrentPosition = () => {
        let isMovePossible = false;
        let positionToCheckFor = +position.subString(1, position.length)

        moves.forEach((move) => {
            if (!isMovePossible) {
                let possiblePosition = move === 1 ? 18 : move === 2 ? 17 : move === 3 ? 16 : move === 4 ? 15 : move === 5 ? 14 : undefined;
                if (possiblePosition) {
                    isMovePossible = positionToCheckFor <= possiblePosition;
                } else if (move === 6 && positionToCheckFor < 14) {
                    isMovePossible = true;
                }
            }
        })

        return isMovePossible;
    }

    let color = isWaitingForDiceRoll ? backgroundColor : highlightColor;

    applyAnimationIfNeeded();

    return (
        <div className="cellbox-container" style={{ backgroundColor: backgroundColor }} onClick={() => {
            if (isMovePossibleFromCurrentPosition()) {
                let piece = getPiece();
                if (piece) {
                    onPieceSelection(piece)
                }
            }
        }}>
            {shouldRenderPiece() && <div className="cellbox-piece" style={{ backgroundColor: getPieceColor() }}></div>}
        </div>
    )
}

export default CellBox
