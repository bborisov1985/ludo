import React from 'react'
import { B10, B11, B12, B13, B6, B7, B8, B9, G1, G14, G15, G16, G17, G18, G2, G3, G4, G5, R1, R14, R15, R16, R17, R18, R2, R3, R4, R5, Y10, Y11, Y12, Y13, Y6, Y7, Y8, Y9 } from '../../util/constants'
import './HorizontalCellsContainer.css'
import Dice from '../Dice/Dice'
import CellBox from '../CellBox/CellBox'
import { getCellBackgroundColor } from '../../util/util'

const HorizontalCellsContainer = ({ isRolling, diceNumber, turn, onDiceRoll, onPieceSelection, red, yellow, green, blue, moves, isWaitingForDiceRoll }) => {

    const renderEmptySpace = () => {
        return <div className="empty-space"></div>
    }

    const renderRow = (positionArray) => {
        return (
            <div className="row-container">
                {positionArray.map((cellPosition) => {
                    return (
                        <div className="hor-cell-container" key={cellPosition}>
                            <CellBox
                                backgroundColor={getCellBackgroundColor(cellPosition)}
                                onPieceSelection={onPieceSelection}
                                red={red}
                                yellow={yellow}
                                green={green}
                                blue={blue}
                                turn={turn}
                                moves={moves}
                                isWaitingForDiceRoll={isWaitingForDiceRoll}
                                position={cellPosition}
                            />
                        </div>
                    )
                })}
            </div>

        )
    }

    const renderRowsContainer = (array1, array2) => {
        return (
            <div className="rows-container">
                {renderRow(array1)}
                {renderEmptySpace()}
                {renderRow(array2)}
            </div>
        )
    }

    return (
        <div className="horizontal-cells-container">
            <Dice
                isRolling={isRolling}
                diceNumber={diceNumber}
                turn={turn}
                onDiceRoll={onDiceRoll}
            />
            {renderRowsContainer([B13, R1, R2, R3, R4, R5], [Y6, Y7, Y8, Y9, Y10, Y11])}
            {renderRowsContainer([B12, R14, R15, R16, R17, R18], [G18, G17, G16, G15, G14, Y12])}
            {renderRowsContainer([B11, B10, B9, B8, B7, B6], [G5, G4, G3, G2, G1, Y13])}
        </div>
    )
}

export default HorizontalCellsContainer
