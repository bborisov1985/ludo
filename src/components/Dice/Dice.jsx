import React from 'react'
import { colors } from '../../util/colors'
import { BLUE, GREEN, RED, YELLOW } from '../../util/constants';
import './Dice.css'
import ReactLoading from 'react-loading';

const Dice = ({ isRolling, turn, onDiceRoll, diceNumber }) => {
    const { red, yellow, green, blue } = colors;
    let color = turn === RED ? red : turn === YELLOW ? yellow : turn === GREEN ? green : turn === BLUE ? blue : undefined;

    const renderSurfaceOne = () => {
        return (
            <div className="dice-dot"></div>
        )
    }

    const renderSurfaceTwo = () => {
        return (
            <div>
                <div className="dice-dot"></div>
                <div className="dice-dot"></div>
            </div>
        )
    }

    const renderSurfaceThree = () => {
        return (
            <div>
                <div className="dice-dot"></div>
                <div className="dice-dot"></div>
                <div className="dice-dot"></div>
            </div>
        )
    }

    const renderSurfaceFour = () => {
        return (
            <div style={{ display: 'flex' }}>
                {renderSurfaceTwo()}
                {renderSurfaceTwo()}
            </div>
        )
    }

    const renderSurfaceFive = () => {
        return (
            <div style={{ display: 'flex' }}>
                {renderSurfaceTwo()}
                {renderSurfaceOne()}
                {renderSurfaceTwo()}
            </div>
        )

    }

    const renderSurfaceSix = () => {
        return (
            <div style={{ display: 'flex' }}>
                {renderSurfaceThree()}
                {renderSurfaceThree()}
            </div>
        )
    }

    const renderDiceSurface = (diceNumber) => {
        switch (diceNumber) {
            case 1:
                return renderSurfaceOne();
            case 2:
                return renderSurfaceTwo();
            case 3:
                return renderSurfaceThree();
            case 4:
                return renderSurfaceFour();
            case 5:
                return renderSurfaceFive();
            case 6:
                return renderSurfaceSix();
            default:
                return true;
        }
    }


    return (
        <div className="dice">
            <h1 className="text">Roll Dice</h1>
            <button className="dice-container" style={{ backgroundColor: color }} onClick={onDiceRoll}>
                {renderDiceSurface(diceNumber)}
            </button>
            {isRolling &&
                <div className="dice-container">
                    <ReactLoading type='spin' />
                </div>
            }
        </div>
    )
}

export default Dice
