import React from 'react'
import './NewGame.css'

const NewGame = ({ isStartGameVisible, onCancel, onRedInput, onYellowInput, onGreenInput, onBlueInput, onStart, red, yellow, green, blue }) => {

    return (
        <div className="new-game">
            {isStartGameVisible &&
                <div className="new-game-container">
                    <div className="create-player-name">
                        <label className="red-label">Red Player:</label>
                        <input className="red-input" type="text" placeholder="Type red palyer name..." onChange={onRedInput} />
                    </div>
                    <div className="create-player-name">
                        <label className="yellow-label">Yellow Player:</label>
                        <input className="yellow-input" type="text" placeholder="Type yellow palyer name..." onChange={onYellowInput} />
                    </div>
                    <div className="create-player-name">
                        <label className="green-label">Green Player:</label>
                        <input className="green-input" type="text" placeholder="Type green palyer name..." onChange={onGreenInput} />
                    </div>
                    <div className="create-player-name">
                        <label className="blue-label">Blue Player:</label>
                        <input className="blue-input" type="text" placeholder="Type blue palyer name..." onChange={onBlueInput} />
                    </div>
                    <div className="create-game-buttons">
                        <button className="create-game-button" onClick={() => {
                            let minPlayerCount = 0;
                            if (red.name !== '') {
                                minPlayerCount++
                            }
                            if (yellow.name !== '') {
                                minPlayerCount++
                            }
                            if (green.name !== '') {
                                minPlayerCount++
                            }
                            if (blue.name !== '') {
                                minPlayerCount++
                            }
                            if (minPlayerCount >= 2) {
                                onStart()
                            } else {
                                alert('At least 2 players are required to start the game!')
                            }

                        }}>New Game</button>
                        <button className="create-game-button" onClick={onCancel}>Cancel</button>
                    </div>

                </div>}
        </div>
    )
}

export default NewGame
