import React, { useState } from 'react'
import { colors } from '../../util/colors'
import './PlayerBox.css'

const PlayerBox = ({ color, customStyle, one, two, three, four, onPieceSelection, animateForSelection }) => {

    const [isAnimating, setIsAnimating] = useState(false);
    const [backgroundColor, setBackgroundColor] = useState(color);
    const [intervalId, setIntervalId] = useState();
    let shouldRenderBackgroundColor = 1;
    const applyAnimationIfNeeded = () => {
        if (animateForSelection) {
            if (!isAnimating) {
                setIsAnimating(true);
                setIntervalId(setInterval(() => {
                    shouldRenderBackgroundColor++;
                    shouldRenderBackgroundColor % 2 === 0 ? setBackgroundColor(color) : setBackgroundColor(colors.white);
                }, 400))
            }
        } else {
            clearInterval(intervalId);
            if (isAnimating) {
                setIsAnimating(false);
                setBackgroundColor(color);
            }
        }
    }
    const renderPiece = (piece) => {
        if (piece.position === 'HOME') {
            return (
                <div className="piece-style" style={{ backgroundColor: color }} onClick={() => { onPieceSelection(piece) }}></div>
            )
        }
        return (
            <div className="piece-style" style={{ backgroundColor: colors.white }}></div>
        )
    }

    const style = {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: backgroundColor,
        height: '100%',
        flex: 4,
        ...customStyle
    }

    applyAnimationIfNeeded();

    return (
        <div style={style}>
            <div className="inner-container">
                <div className="pieces-container">
                    {renderPiece(one)}
                    {renderPiece(two)}
                </div>
                <div className="pieces-container">
                    {renderPiece(three)}
                    {renderPiece(four)}
                </div>
            </div>
        </div>
    )
}

export default PlayerBox
