import './App.css';
import { useState } from 'react';
import Game from './screens/Game/Game'
import Home from './screens/Home/Home'

const App = () => {

  const [isGameInProgress, setisGameInProgress] = useState(false);
  const [isStartGameVisible, setIsStartGameVisible] = useState(false);
  const [red, setRed] = useState({ name: '' });
  const [yellow, setYellow] = useState({ name: '' });
  const [green, setGreen] = useState({ name: '' });
  const [blue, setBlue] = useState({ name: '' });

  return (
    <div className="ludo">
      {isGameInProgress
        ? <Game
          redName={red.name}
          yellowName={yellow.name}
          greenName={green.name}
          blueName={blue.name}
        />
        : <Home
          isStartGameVisible={isStartGameVisible}
          onNewGameClick={() => setIsStartGameVisible(true)}
          onCancel={() => setIsStartGameVisible(false)}
          onStart={() => {
            setisGameInProgress(true)
            setIsStartGameVisible(false)
          }}
          red={red}
          yellow={yellow}
          green={green}
          blue={blue}
          onRedInput={(name) => setRed({ ...red, name: name })}
          onYellowInput={(name) => setYellow({ ...yellow, name: name })}
          onGreenInput={(name) => setGreen({ ...green, name: name })}
          onBlueInput={(name) => setBlue({ ...blue, name: name })}
        />
      }
    </div>
  );
}

export default App;
