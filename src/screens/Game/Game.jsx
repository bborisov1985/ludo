import React, { useState } from 'react'
import './Game.css'
import PlayerBox from '../../components/PlayerBox/PlayerBox'
import { colors } from '../../util/colors'
import { B1, B9, BLUE, BOTTOM_VERTICAL, FINISHED, FOUR, G1, G9, GREEN, HOME, ONE, R1, R9, RED, THREE, TOP_VERTICAL, TWO, Y1, Y9, YELLOW } from '../../util/constants'
import VerticalCellsContainer from '../../components/VerticalCellsContainer/VerticalCellsContainer'
import HorizontalCellsContainer from '../../components/HorizontalCellsContainer/HorizontalCellsContainer'

const Game = ({ redName, yellowName, greenName, blueName }) => {

    const initPieces = (playerColor) => {
        let time = new Date().getTime();
        return {
            one: { name: ONE, color: playerColor, position: HOME, updateTime: time },
            two: { name: TWO, color: playerColor, position: HOME, updateTime: time },
            three: { name: THREE, color: playerColor, position: HOME, updateTime: time },
            four: { name: FOUR, color: playerColor, position: HOME, updateTime: time }
        }
    }

    const initPlayer = (playerType, color) => {
        return {
            pieces: initPieces(playerType),
            color: color,
            player: playerType
        }
    }

    const [red, setRed] = useState(initPlayer(RED, colors.red))
    const [yellow, setYelow] = useState(initPlayer(YELLOW, colors.yellow))
    const [green, setGreen] = useState(initPlayer(GREEN, colors.green))
    const [blue, setBlue] = useState(initPlayer(BLUE, colors.blue))
    const [isRolling, setIsRolling] = useState(false);
    const [diceNumber, setDiceNumber] = useState(4);
    const [moves, setMoves] = useState([]);
    const [animateForSelection, setAnimateForSelection] = useState(false);
    const [bonusCount, setBonusCount] = useState(0);
    const [isWaitingForDiceRoll, setIsWaitingForDiceRoll] = useState(true);
    const [turn, setTurn] = useState(redName !== '' ? RED : yellowName !== '' ? YELLOW : greenName !== '' ? GREEN : blueName !== '' ? BLUE : undefined)
    const renderPlayerBox = (player, customStyle) => {
        const { one, two, three, four } = player.pieces;
        customStyle.opacity = turn === player.player ? 1 : 0.6;
        let hasSix = moves.filter(move => move === 6).length > 0;

        return (
            <PlayerBox
                color={player.color}
                one={one}
                two={two}
                three={three}
                four={four}
                customStyle={customStyle}
                onPieceSelection={(selectedPiece) => {
                    if (turn === player.player) {
                        onPieceSelection(selectedPiece);
                    }
                }}
                animateForSelection={animateForSelection && turn === player.player && hasSix}
            />
        )
    }

    const getRandomInt = () => {
        let randomInt = Math.floor(Math.random() * Math.floor(6));
        return randomInt + 1;
    }

    const onDiceRoll = () => {
        setIsRolling(true);
        setDiceNumber(getRandomInt());
        setTimeout(() => {
            const updatedMoves = [...moves, diceNumber];
            if (diceNumber === 6) {
                if (updatedMoves.length === 3) {
                    setIsRolling(false);
                    setMoves([]);
                    setTurn(getNextTurn());
                } else {
                    setIsRolling(false);
                    setMoves(updatedMoves);
                }
            } else {
                setIsWaitingForDiceRoll(false);
                setIsRolling(false);
                setMoves(updatedMoves);
                updatePlayerPieces(turn);
            }
        }, 500)
    }

    const isPlayerFinished = (player) => {
        const { one, two, three, four } = player.pieces;
        return one.position === FINISHED && two.position === FINISHED && three.position === FINISHED && four.position === FINISHED
    }

    const getNextTurn = () => {
        setIsWaitingForDiceRoll(true);
        let isRedNext = redName !== '' && !isPlayerFinished(red);
        let isYellowNext = yellowName !== '' && !isPlayerFinished(yellow);
        let isGreenNext = greenName !== '' && !isPlayerFinished(green);
        let isBlueNext = blueName !== '' && !isPlayerFinished(blue);

        if (bonusCount > 0) {
            setBonusCount(bonusCount - 1);
            if (isPlayerFinished(turn)) {
                return turn;
            }
        }

        switch (turn) {
            case RED:
                return isYellowNext ? YELLOW : isGreenNext ? GREEN : isBlueNext ? BLUE : undefined;
            case YELLOW:
                return isGreenNext ? GREEN : isBlueNext ? BLUE : isRedNext ? RED : undefined;
            case GREEN:
                return isBlueNext ? BLUE : isRedNext ? RED : isYellowNext ? YELLOW : undefined;
            case BLUE:
                return isRedNext ? RED : isYellowNext ? YELLOW : isGreenNext ? GREEN : undefined;
            default:
                return turn;

        }
    }

    const updatePlayerPieces = (player) => {
        if (moves.length >= 1) {
            if (!isPlayerFinished(player)) {
                if (playerHasOptionsForMoves(player)) {
                    setAnimateForSelection(true);
                } else if (playerHasSinglePossibleMove(player)) {
                    if (playerHasSingleUnfinishedPiece(player)) {
                        let singlePossibleMove = getSinglePossibleMove(player);
                        if (singlePossibleMove) {
                            const indexOf = moves.indexOf(singlePossibleMove.move);
                            if (indexOf > -1) {
                                const removedMoves = [...moves].splice(indexOf, 1);
                                setMoves(removedMoves);
                            }
                            movePieceByPosition(singlePossibleMove.move, singlePossibleMove.piece);
                        }
                    } else {
                        if (moves.length === 1) {
                            let piece = getPieceWithPossibleMove(player);
                            movePieceByPosition(piece, moves.shift())
                        } else {
                            setAnimateForSelection(true);
                        }
                    }
                } else {
                    setTurn(getNextTurn());
                    setMoves([]);
                    setAnimateForSelection(false);
                }
            } else {
                setTurn(getNextTurn());
                setMoves([]);
                setAnimateForSelection(false);
            }
        } else {
            setTurn(getNextTurn());
            setAnimateForSelection(false);
        }

    }

    const playerHasOptionsForMoves = (player) => {
        let countOfMoveOptions = getCountMoveOptions(player);
        return countOfMoveOptions > 1
    }

    const getCountMoveOptions = (player) => {
        const { one, two, three, four } = player.pieces;
        let hasSix = moves.filter(move => move === 6).length > 0;
        let countOfOptions = 0;

        const isMovePossibleForPosition = (position) => {
            if (position === FINISHED) {
                return false;
            }
            if (position === HOME) {
                if (hasSix) {
                    return true;
                }
                return false;
            }

            let isMovePossible = false;
            let positionToCheckFor = +position.subString(1, position.length)

            moves.forEach((move) => {
                if (!isMovePossible) {
                    let possiblePosition = move === 1 ? 18 : move === 2 ? 17 : move === 3 ? 16 : move === 4 ? 15 : move === 5 ? 14 : undefined;
                    if (possiblePosition) {
                        isMovePossible = positionToCheckFor <= possiblePosition;
                    } else if (move === 6 && positionToCheckFor < 14) {
                        isMovePossible = true;
                    }
                }
            })

            return isMovePossible;
        }

        isMovePossibleForPosition(one.position) && countOfOptions++;
        isMovePossibleForPosition(two.position) && countOfOptions++;
        isMovePossibleForPosition(three.position) && countOfOptions++;
        isMovePossibleForPosition(four.position) && countOfOptions++;

        return countOfOptions;
    }

    const playerHasSinglePossibleMove = (player) => {
        let countOfMoveOptions = getCountMoveOptions(player);
        return countOfMoveOptions === 1;
    }

    const playerHasSingleUnfinishedPiece = (player) => {
        const { one, two, three, four } = player.pieces;
        let countOfUnfinishedPieces = 0;

        one.position !== FINISHED && countOfUnfinishedPieces++;
        two.position !== FINISHED && countOfUnfinishedPieces++;
        three.position !== FINISHED && countOfUnfinishedPieces++;
        four.position !== FINISHED && countOfUnfinishedPieces++;

        return countOfUnfinishedPieces === 1;
    }

    const getSinglePossibleMove = (player) => {
        const { one, two, three, four } = player.pieces;
        let hasSix = moves.filter(move => move === 6).length > 0;
        let possibleMove;

        const isMovePossibleForPosition = (position) => {
            if (position === FINISHED) {
                return false;
            }
            if (position === HOME) {
                if (hasSix) {
                    return true;
                }
                return false;
            }

            let isMovePossible = false;
            let positionToCheckFor = +position.subString(1, position.length)

            moves.forEach((move) => {
                if (!isMovePossible) {
                    let possiblePosition = move === 1 ? 18 : move === 2 ? 17 : move === 3 ? 16 : move === 4 ? 15 : move === 5 ? 14 : undefined;
                    if (possiblePosition) {
                        isMovePossible = positionToCheckFor <= possiblePosition;
                        if (isMovePossible) {
                            possibleMove = move;
                        }
                    } else if (move === 6 && positionToCheckFor < 14) {
                        isMovePossible = true;
                        possibleMove = moves;
                    }
                }
            })

            return isMovePossible;
        }

        if (isMovePossibleForPosition(one.position)) {
            return {
                move: possibleMove,
                piece: one
            }
        }
        if (isMovePossibleForPosition(one.position)) {
            return {
                move: possibleMove,
                piece: two
            }
        }
        if (isMovePossibleForPosition(one.position)) {
            return {
                move: possibleMove,
                piece: three
            }
        }
        if (isMovePossibleForPosition(one.position)) {
            return {
                move: possibleMove,
                piece: four
            }
        }

        return undefined;

    }

    const getPieceWithPossibleMove = (player) => {
        const { one, two, three, four } = player.pieces;
        let hasSix = moves.filter(move => move === 6).length > 0;

        const isMovePossibleForPosition = (position) => {
            if (position === FINISHED) {
                return false;
            }
            if (position === HOME) {
                if (hasSix) {
                    return true;
                }
                return false;
            }

            let isMovePossible = false;
            let positionToCheckFor = +position.subString(1, position.length)

            moves.forEach((move) => {
                if (!isMovePossible) {
                    let possiblePosition = move === 1 ? 18 : move === 2 ? 17 : move === 3 ? 16 : move === 4 ? 15 : move === 5 ? 14 : undefined;
                    if (possiblePosition) {
                        isMovePossible = positionToCheckFor <= possiblePosition;
                    } else if (move === 6 && positionToCheckFor < 14) {
                        isMovePossible = true;
                    }
                }
            })

            return isMovePossible;
        }

        if (isMovePossibleForPosition(one.position)) {
            return one
        }
        if (isMovePossibleForPosition(one.position)) {
            return two
        }
        if (isMovePossibleForPosition(one.position)) {
            return three
        }
        if (isMovePossibleForPosition(one.position)) {
            return four
        }

        return undefined;
    }

    const movePieceByPosition = (piece, move) => {
        let newPosition = '';
        let position = +piece.position.subString(1, piece.position.length);
        let cellAreaIndicator = piece.position.subString(0, 1);

        if (piece.position === HOME && move === 6) {
            newPosition = piece.color === RED ? R1 : piece.color === YELLOW ? Y1 : piece.color === GREEN ? G1 : position.color === BLUE ? B1 : undefined;
        } else if (position <= 13) {
            if ((cellAreaIndicator === 'B' && piece.color === RED) ||
                (cellAreaIndicator === 'R' && piece.color === YELLOW) ||
                (cellAreaIndicator === 'Y' && piece.color === GREEN) ||
                (cellAreaIndicator === 'G' && piece.color === BLUE)
            ) {
                if (position + move <= 12) {
                    newPosition = cellAreaIndicator + (position + move);
                } else {
                    let updatedPosition = (position + move + 1);
                    if (updatedPosition === 19) {
                        newPosition = FINISHED;
                    } else {
                        let updatedCellAreaIndicator = cellAreaIndicator === 'R' ? 'Y' : cellAreaIndicator === 'Y' ? 'G' : cellAreaIndicator === 'G' ? 'B' : cellAreaIndicator === 'B' ? 'R' : undefined;
                        newPosition = updatedCellAreaIndicator + updatedPosition;
                    }
                }
            } else {
                if (position + move <= 13) {
                    newPosition = cellAreaIndicator + (position + move);
                } else {
                    let nextPosition = (position + move) - 13;
                    let updatedCellAreaIndicator = cellAreaIndicator === 'R' ? 'Y' : cellAreaIndicator === 'Y' ? 'G' : cellAreaIndicator === 'G' ? 'B' : cellAreaIndicator === 'B' ? 'R' : undefined;
                    newPosition = updatedCellAreaIndicator + nextPosition;
                }
            }
        } else {
            if (position + move <= 19) {
                if (position + move === 19) {
                    newPosition = FINISHED;
                } else {
                    newPosition = cellAreaIndicator + (position + move);
                }
            }
        }

        if (newPosition !== '') {
            piece.position = newPosition;
            piece.updateTime = new Date().getTime();
        }

        if (didGetBonusWithNewPosition(piece) && isPlayerFinished(piece.color)) {
            setBonusCount(bonusCount + 1, () => {
                if (moves.length === 1) {
                    updatePlayerPieces(piece.color)
                } else if (moves.length === 0 || isPlayerFinished(piece.color)) {
                    setAnimateForSelection(false);
                    setMoves([]);
                    setTurn(getNextTurn());
                }
            });
        } else {
            if (moves.length === 1) {
                updatePlayerPieces(piece.color)
            } else if (moves.length === 0 || isPlayerFinished(piece.color)) {
                setAnimateForSelection(false);
                setMoves([]);
                setTurn(getNextTurn());
            }
        }
    }

    const didGetBonusWithNewPosition = (piece) => {

        const checkIfPositionMatchesExistingPiece = (piece, player) => {
            const { one, two, three, four } = player.pieces;
            let positionMatched = false;

            if (piece.position === one.position) {
                one.position = HOME;
                positionMatched = true;
            }
            if (piece.position === two.position) {
                two.position = HOME;
                positionMatched = true;
            }
            if (piece.position === three.position) {
                three.position = HOME;
                positionMatched = true;
            }
            if (piece.position === four.position) {
                four.position = HOME;
                positionMatched = true;
            }

            return positionMatched;
        }

        if (piece.position === FINISHED) {
            return true;
        }
        if (piece.position === R1 ||
            piece.position === R9 ||
            piece.position === Y1 ||
            piece.position === Y9 ||
            piece.position === G1 ||
            piece.position === G9 ||
            piece.position === B1 ||
            piece.position === B9
        ) {
            return false;
        }

        if (piece.color !== red.player && checkIfPositionMatchesExistingPiece(piece, red)) {
            return true
        }
        if (piece.color !== yellow.player && checkIfPositionMatchesExistingPiece(piece, yellow)) {
            return true
        }
        if (piece.color !== green.player && checkIfPositionMatchesExistingPiece(piece, green)) {
            return true
        }
        if (piece.color !== blue.player && checkIfPositionMatchesExistingPiece(piece, blue)) {
            return true
        }
        return false;
    }

    const onPieceSelection = (selectedPiece) => {
        if (isWaitingForDiceRoll) {
            return;
        }
        const player = selectedPiece.color;
        const { one, two, three, four } = player.pieces;

        if (moves.length === 1) {
            if (selectedPiece.position === HOME && moves[0] !== 6) {
                return;
            }
            movePieceByPosition(selectedPiece, moves.shift());
        } else if (moves.length > 1) {
            if (selectedPiece.position === HOME) {
                moves.shift();
                selectedPiece.position = selectedPiece.color === RED ? R1 : selectedPiece.color === YELLOW ? Y1 : selectedPiece.color === GREEN ? G1 : selectedPiece.color === BLUE ? B1 : undefined;
                selectedPiece.updateTime = new Date().getTime();
                if (moves.length === 1) {
                    if (playerHasOptionsForMoves(player)) {
                        movePieceByPosition(selectedPiece, moves.shift());
                    } else {
                        const isActivePiece = piece => piece.position !== HOME && piece.position !== FINISHED;
                        let activePieces = [];

                        isActivePiece(one) && activePieces.push(one);
                        isActivePiece(two) && activePieces.push(two);
                        isActivePiece(three) && activePieces.push(three);
                        isActivePiece(four) && activePieces.push(four);

                        let isSamePositionForAllActivePieces = activePieces.every(activePiece => activePiece.positoin === activePieces[0].position);
                        if (isSamePositionForAllActivePieces) {
                            movePieceByPosition(selectedPiece, moves.shift())
                        }
                    }
                }
            } else {
                const onMoveSelected = (selectedMove) => {
                    if (isMovePossibleForPosition(selectedPiece.position, selectedMove)) {
                        const index = moves.indexOf(+selectedMove);
                        if (index > -1) {
                            moves.splice(index, 1);
                        }
                        movePieceByPosition(selectedPiece, selectedMove);
                    } else {
                        alert('Move not possible!')
                    }
                }

                let moveOptions = [];
                let optionOne = moves[0].toString();
                moveOptions.push({ tex: optionOne, onClick: () => { onMoveSelected(optionOne) } })
                if (moves.length > 1) {
                    let optionTwo = moves[1].toString();
                    optionTwo && moveOptions.push({ tex: optionTwo, onClick: () => { onMoveSelected(optionTwo) } })
                }
                if (moves.length > 2) {
                    let optionThree = moves[2].toString();
                    optionThree && moveOptions.push({ tex: optionThree, onClick: () => { onMoveSelected(optionThree) } })
                }
                alert('Please select your move!')
            }
        }
    }

    const isMovePossibleForPosition = (position, move) => {
        let isMovePossible = false;
        let positionToCheckFor = +position.subString(1, position.length)

        let possiblePosition = move === 1 ? 18 : move === 2 ? 17 : move === 3 ? 16 : move === 4 ? 15 : move === 5 ? 14 : undefined;
        if (possiblePosition) {
            isMovePossible = positionToCheckFor <= possiblePosition;
        } else if (move === 6 && positionToCheckFor < 14) {
            isMovePossible = true;
        }

        return isMovePossible;
    }


    return (
        <div className="game-screen">
            <div className="game-container">
                <div className="two-players-container">
                    {renderPlayerBox(red, { borderTopLeftRadius: '20px' })}
                    <VerticalCellsContainer
                        position={TOP_VERTICAL}
                        red={red}
                        yellow={yellow}
                        green={green}
                        blue={blue}
                        turn={turn}
                        moves={moves}
                        isWaitingForDiceRoll={isWaitingForDiceRoll}
                        onPieceSelection={(selectedPiece) => onPieceSelection(selectedPiece)}
                    />
                    {renderPlayerBox(yellow, { borderTopRightRadius: '20px' })}
                </div>
                <HorizontalCellsContainer
                    isRolling={isRolling}
                    diceNumber={diceNumber}
                    turn={turn}
                    onDiceRoll={() => onDiceRoll()}
                    red={red}
                    yellow={yellow}
                    green={green}
                    blue={blue}
                    moves={moves}
                    isWaitingForDiceRoll={isWaitingForDiceRoll}
                    onPieceSelection={(selectedPiece) => onPieceSelection(selectedPiece)}
                />
                <div className="two-players-container">
                    {renderPlayerBox(blue, { borderBottomLeftRadius: '20px' })}
                    <VerticalCellsContainer
                        position={BOTTOM_VERTICAL}
                        red={red}
                        yellow={yellow}
                        green={green}
                        blue={blue}
                        turn={turn}
                        moves={moves}
                        isWaitingForDiceRoll={isWaitingForDiceRoll}
                        onPieceSelection={(selectedPiece) => onPieceSelection(selectedPiece)}
                    />
                    {renderPlayerBox(green, { borderBottomRightRadius: '20px' })}
                </div>
            </div>
        </div>
    )
}

export default Game
