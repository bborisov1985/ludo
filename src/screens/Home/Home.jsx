import React from 'react'
import './Home.css'
import Ludo from '../../images/Ludo.png'
import NewGame from '../../components/NewGame/NewGame'

const Home = ({ isStartGameVisible, onNewGameClick, onCancel, onRedInput, onYellowInput, onGreenInput, onBlueInput, red, yellow, green, blue, onStart }) => {
    return (
        <div className="home-screen">
            <h1 className="home-screen-logo">Ludo Classic</h1>
            <div className="home-screen-container">
                <button className="home-screen-button" onClick={onNewGameClick}>New Game</button>
                <img className="home-screen-image" src={Ludo} alt="Ludo" />
            </div>
            <NewGame
                isStartGameVisible={isStartGameVisible}
                onCancel={onCancel}
                onRedInput={onRedInput}
                onYellowInput={onYellowInput}
                onGreenInput={onGreenInput}
                onBlueInput={onBlueInput}
                red={red}
                yellow={yellow}
                green={green}
                blue={blue}
                onStart={onStart}
            />
        </div>
    )
}

export default Home
