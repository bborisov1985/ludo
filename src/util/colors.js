export const colors = {
    red: '#d41426',
    yellow: '#ffd520',
    green: '#5bbc19',
    blue: '#0b1e58', 
    white: '#f5f5f6'
}